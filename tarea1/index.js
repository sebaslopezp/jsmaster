// Ejercicio 1
console.log("ejercicio 1 ");
function numero(number){
    resultado = number % 2;
    if(resultado == 0){
        console.log("es par");
    }else{
        console.log("es impar");
    }
    return resultado;
}

numero(2);


// Ejercicio 2
console.log("ejercicio 2");
var string  = "abcdefghijklmnñopqrstuvwxyz";
function position(caracter){
    var n = string.indexOf(caracter);
    console.log(n);
}
position("e");

// Ejercicio 3
console.log("ejercicio 3");
var impuesto = 0.19;
function calcular(precio){
    var n = precio * impuesto;
    var total = Number(precio) + Number(n);
    console.log(total);
    return total;
}
calcular(1000);

// Ejercicio 4
console.log("ejercicio 4");
function getRandomInt(nombre) {
    function dado(max,min){
        var aleatorio = Math.round(Math.random()*(max-min)+parseInt(min));
        var resultado = nombre + " tiró un dado y salió " + aleatorio;
        console.log(resultado);
    }
    dado(1,6);
}


getRandomInt("Seba");
getRandomInt("Seba");
getRandomInt("Seba");