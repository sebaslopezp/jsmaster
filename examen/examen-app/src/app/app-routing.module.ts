import { LocationsComponent } from './pages/locations/locations.component';
import { HomeComponent } from './pages/home/home.component';
import { CharacterInfoComponent } from './pages/character-info/character-info.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full',
  },
  {
    path: 'home',
    component: HomeComponent,
  },
  {
    path: 'info/:id',
    component: CharacterInfoComponent,
  },
  {
    path: 'locations',
    component: LocationsComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
