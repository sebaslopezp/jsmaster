import { Component, Input, OnInit, Output } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { CharacterService } from 'src/app/core/services/character.service';

@Component({
  selector: 'app-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.scss'],
})
export class PaginatorComponent implements OnInit {
  @Input() info: any;
  page: number;
  @Output() characters: any;
  constructor(private characterService: CharacterService) {}

  ngOnInit(): void {
    console.log('esta es la info', this.info);
  }

  OnChangePage(event: PageEvent) {
    console.log('pagina cambiada', event);
    this.page = event.pageIndex + 1;
    this.characterService.getCharacterPerPage(this.page).subscribe((data) => {
      this.characters = data;
      console.log(data);
    });
  }
}
