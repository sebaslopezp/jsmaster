import { Component, OnInit } from '@angular/core';
import { CharacterService } from 'src/app/core/services/character.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
@Component({
  selector: 'app-character-info',
  templateUrl: './character-info.component.html',
  styleUrls: ['./character-info.component.scss'],
})
export class CharacterInfoComponent implements OnInit {
  characterId: any;
  characterInfo: any;
  constructor(
    private characterService: CharacterService,
    private activatedRoute: ActivatedRoute,
    private location: Location
  ) {
    this.characterId = this.activatedRoute.snapshot.paramMap.get('id');
  }

  ngOnInit(): void {
    console.log(this.characterId);
    this.getCharacterInfo();
  }

  getCharacterInfo() {
    this.characterService
      .getCharacterInfo(this.characterId)
      .subscribe((data) => {
        console.log(data);
        this.characterInfo = data;
        localStorage.setItem(
          'character-info',
          JSON.stringify(this.characterInfo)
        );
      });
  }

  back(): void {
    this.location.back();
  }
}
