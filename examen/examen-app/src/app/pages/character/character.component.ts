import { Router } from '@angular/router';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { CharacterService } from 'src/app/core/services/character.service';
import { PageEvent } from '@angular/material/paginator';

@Component({
  selector: 'app-character',
  templateUrl: './character.component.html',
  styleUrls: ['./character.component.scss'],
})
export class CharacterComponent implements OnInit {
  characters: any;
  page: number;
  constructor(
    private characterService: CharacterService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.getCharacters();
  }

  getCharacters() {
    //
    this.characterService.getCharacters().subscribe((data: any) => {
      this.characters = data;
      localStorage.setItem('characters', JSON.stringify(this.characters));
      console.log(this.characters);
    });
  }

  seeCharacter(id: number) {
    this.router.navigate(['/info', id]);
  }

  OnChangePage(event: PageEvent) {
    console.log('pagina cambiada', event);
    this.page = event.pageIndex + 1;
    this.characterService.getCharacterPerPage(this.page).subscribe((data) => {
      this.characters = data;
      console.log(data);
    });
  }
}
