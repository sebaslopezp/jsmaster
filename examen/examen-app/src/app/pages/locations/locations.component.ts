import { Component, OnInit } from '@angular/core';
import { LocationsService } from 'src/app/core/services/locations.service';

@Component({
  selector: 'app-locations',
  templateUrl: './locations.component.html',
  styleUrls: ['./locations.component.scss'],
})
export class LocationsComponent implements OnInit {
  locations: any;
  words: any;
  types: any;
  firstPlanet: any;
  firstCluster: any;
  message: string;
  timeLeft: number;
  interval;
  constructor(private locationService: LocationsService) {}

  ngOnInit(): void {
    this.getLocations();
    this.message = 'Este mensaje cambiará';

    setTimeout(() => {
      this.message = 'Mensaje Cambiado';
    }, 6000);

    this.startTimer();
  }

  getLocations() {
    this.locationService.getLocations().subscribe((data) => {
      console.log(data);
      this.locations = data;
      localStorage.setItem('location-info', JSON.stringify(this.locations));
      console.log(typeof this.locations.results);
      this.words = this.locations.results.filter(
        (word) => word.name.length > 26
      );

      this.types = this.locations.results.filter(
        (word) => word.type == 'Planet'
      );

      this.firstPlanet = this.locations.results.find(
        (element) => element.type == 'Planet'
      );

      this.firstCluster = this.locations.results.find(
        (element) => element.type == 'Cluster'
      );
    });
  }

  startTimer() {
    this.interval = setInterval(() => {
      if (this.timeLeft > 0) {
        this.timeLeft--;
      } else {
        this.timeLeft = 60;
      }
    }, 1000);
  }
}
