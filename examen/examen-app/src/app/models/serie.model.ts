export class Info {
  count: number;
  pages: number;
  next: string;
  prev?: any;
}

export class Origin {
  name: string;
  url: string;
}

export class Location {
  id: number;
  name: string;
  type: string;
  dimension: string;
}

export class Result {
  id: number;
  name: string;
  status: string;
  species: string;
  type: string;
  gender: string;
  origin: Origin;
  location: Location;
  image: string;
  episode: string[];
  url: string;
  created: Date;
}

export class RootObject {
  info: Info;
  results: Result[];
}
