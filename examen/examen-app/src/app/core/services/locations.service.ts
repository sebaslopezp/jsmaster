import { environment } from '../../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import * as SerieModel from 'src/app/models/serie.model';

@Injectable({
  providedIn: 'root',
})
export class LocationsService {
  private readonly url = environment.API_URL;
  constructor(private http: HttpClient) {}

  getLocations() {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options = {
      headers: headers,
    };
    return this.http
      .get<SerieModel.Location>(`${this.url}location`, options)
      .pipe(map((res) => res));
  }
}
