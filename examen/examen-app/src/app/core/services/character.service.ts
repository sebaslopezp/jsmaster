import { environment } from '../../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import * as SerieModel from 'src/app/models/serie.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class CharacterService {
  private readonly url = environment.API_URL;
  constructor(private http: HttpClient) {}

  getCharacterInfo(id: number): Observable<SerieModel.Result> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options = {
      headers: headers,
    };
    return this.http
      .get<SerieModel.Result>(`${this.url}character/${id}`, options)
      .pipe(map((res) => res));
  }

  getCharacters(): Observable<SerieModel.Result> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options = {
      headers: headers,
    };
    return this.http
      .get<SerieModel.Result>(`${this.url}character`, options)
      .pipe(map((res) => res));
  }

  getCharacterPerPage(page?: number) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options = {
      headers: headers,
    };
    return this.http
      .get<SerieModel.Result>(`${this.url}character/?page=${page}`, options)
      .pipe(map((res) => res));
  }

  getLocations() {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options = {
      headers: headers,
    };
    return this.http
      .get<SerieModel.Result>(`${this.url}locations`, options)
      .pipe(map((res) => res));
  }
}
