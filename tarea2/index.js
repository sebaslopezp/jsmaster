
const mascotas = ['perros','gatos','canarios','pez','serpiente'];
let esSegura = false;

function llamarAlerta(parametro){
   alert(parametro);
}

function esHttps(){
    let foo = 'https://escalab-6b3e3.firebaseio.com/javascript.json';
    if(foo.startsWith('https')){
        esSegura = true;
    }else{
        esSegura = false;
    }
    return esSegura;
}

function eliminarMascota(tipoMascota){
    const tipo = tipoMascota;
    const position = mascotas.indexOf(tipo);
    mascotas.splice(position,1);
    return mascotas;
}

function eliminarUltimaMascota(){
    let last = mascotas[mascotas.length-1];
    mascotas.pop(last);
    return mascotas;
}

function agregarMascota(tipoMascota){
    mascotas.push(tipoMascota);
    return mascotas;
}

function contadorMascotas(){
    const substring = 'os'
    const el = mascotas.filter(a => a.includes(substring));
    return 'En el arreglo hay ' + el.length + ' que terminan con '+ substring;
}


